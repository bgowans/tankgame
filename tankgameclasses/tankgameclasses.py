# Kurt Davis and Bill Gowans
# 5/4/16
# PyGame and Twisted Milestone
# Class file for TankGame

import sys
import os
import pygame
from pygame.locals import *
from math import *
from copy import deepcopy

tank_fire = "tank_fire.wav"

images_directory = 'images/'
sounds_directory = 'sounds/'


# Set of all tile types with their respective attributes
TileSet = {
		1: ('grass', False, 'grass.png'),
		2: ('rock', True, 'rock.png'),
		3: ('mud', False, 'mud.png'),
		4: ('road', False, 'road.png')
	}

class Tile(pygame.sprite.Sprite):
	# The Tile class is the basic blocks of each level
	# Each type of tile has a different image and can either be collidable with tanks/shells or not
	# When a tile is created, the tile image is loaded and its center is aligned with the inputted cooridinate for easy screen placement
	def __init__(self, tiletype, posX, posY):
		self.tiletype = int(tiletype)

		self.typename = TileSet[self.tiletype][0]
		self.collidable = TileSet[self.tiletype][1]
		self.texFile = TileSet[self.tiletype][2]

		self.image = pygame.image.load(images_directory + self.texFile)
		self.rect = self.image.get_rect()
		self.posX = posX
		self.posY = posY
		self.rect.center = ((20 + (40 * self.posX)), (20 + (40 * self.posY)))

	def __str__(self):
		return str(self.tiletype)

	def __repr__(self):
		return str(self.tiletype)

class Level(object):
	# The Level object creates a composition of tiles based on an inputted file of tiles with their respective positions and types
	# The object stores all tiles in a list of lists, where every element in the list is a row of tiles
	# The object also keeps track of all collidable tiles, important for collision detection later
	def __init__(self, target_file, gs=None):
		self.gs = gs
		self.target_file = target_file
	
		self.data = []
		self.colliders = []
		self.load_file()	

	def load_file(self):
		x = 0
		y = 0
		self.data = []
		infile = open(self.target_file, 'r')
		for line in infile:
			dataline = []
			x = 0
			for tiledata in (line.strip()).split(' '):
				dataline.append(Tile(tiledata, x, y))
				x = x + 1
			self.data.append(dataline[:])
			y = y + 1
		infile.close()

		self.colliders = []
		for line in self.data:
			for tile in line:
				if tile.collidable:
					self.colliders.append(tile)

	def print_data(self):
		""" Function used for debugging """
		for line in self.data:
			print " ".join(str(x) for x in line)

class Player(pygame.sprite.Sprite):
	# Class that holds all tank data, images, ticks, movement, etc.
	# Keeps track of tank positioning, player number, and amount of health the tank has
	# Holds logic for changing tank-specific data, including health adjusting, health bar ajusting, and position/angle adjusting
	def __init__(self, number, gs=None):
		pygame.sprite.Sprite.__init__(self)
		
		self.number = number
		self.gs = gs
		
		self.health = 5
		self.alive = True

		self.damage_cooldown = 0
		
		self.hp_images = []
	
		# Loads images of tanks, health bars, and victory messages
		if self.number is 1:
			self.image = pygame.image.load(images_directory + "tankbody.png")
			self.turret_image = pygame.image.load(images_directory + "tankturret.png")

			self.hp_images.append(pygame.image.load(images_directory + "health_0.png"))
			self.hp_images.append(pygame.image.load(images_directory + "red_health_1.png"))
			self.hp_images.append(pygame.image.load(images_directory + "red_health_2.png"))
			self.hp_images.append(pygame.image.load(images_directory + "red_health_3.png"))
			self.hp_images.append(pygame.image.load(images_directory + "red_health_4.png"))
			self.hp_images.append(pygame.image.load(images_directory + "red_health_5.png"))
		
			self.victory_img = pygame.image.load(images_directory + 'red_wins.png')
		else:
			self.image = pygame.image.load(images_directory + "tankbody2.png")
			self.turret_image = pygame.image.load(images_directory + "tankturret2.png")

			self.hp_images.append(pygame.image.load(images_directory + "health_0.png"))
			self.hp_images.append(pygame.image.load(images_directory + "blue_health_1.png"))
			self.hp_images.append(pygame.image.load(images_directory + "blue_health_2.png"))
			self.hp_images.append(pygame.image.load(images_directory + "blue_health_3.png"))
			self.hp_images.append(pygame.image.load(images_directory + "blue_health_4.png"))
			self.hp_images.append(pygame.image.load(images_directory + "blue_health_5.png"))

			self.victory_img = pygame.image.load(images_directory + 'blue_wins.png')
			
		self.victory_rect = self.victory_img.get_rect()

		#initializes positioning of the tank's turret
		self.rect = self.image.get_rect()
		self.turret_rect = self.turret_image.get_rect()
		self.orig_image = self.image		# Preserve original image		
		self.turret_orig_image = self.turret_image

		#sets the tank's current health bar image
		self.current_health_img = self.hp_images[self.health]
		self.health_rect = self.current_health_img.get_rect()

		#positions tank's health bar based on which player the tank belongs to
		if self.number is 1:
			self.health_rect.center = (100, 20)
		else:
			self.health_rect.center = (1180, 20)

		#chooses sound file for shell firing noise
		self.fire_noise = pygame.mixer.Sound(sounds_directory + tank_fire)

		self.radius = 0.5 * (self.rect.topright[0] - self.rect.topleft[0])

		#initializes several variables used in member functions
		self.firing = False
		self.velocity = 3

		self.fire_delay = 0
	
		self.direction_angle = 0
		self.turret_direction_angle = 0
		
		#binds key booleans to a tuple for efficient key state identification
		self.controls = (pygame.K_w, pygame.K_s, pygame.K_a, pygame.K_d)

	def tick(self,keys):
		""" This function controls tank movement, shell firing, and collision based on inputted key state """

		#cooldown prevents damage being taken more than once per shell colliding with tank
		if self.damage_cooldown > 0:
			self.damage_cooldown = self.damage_cooldown - 1

		#moves the tank an amount specified by velocity in the direction determined by the state of the W,A,S,D keys
                move = [0, 0]
                if keys[self.controls[0]] and keys[self.controls[2]]:
			move[1] -= self.velocity
			move[0] -= self.velocity
			self.direction_angle = degrees(-3*pi/4)
                elif keys[self.controls[0]] and keys[self.controls[3]]:
			move[1] -= self.velocity
			move[0] += self.velocity
			self.direction_angle = degrees(-pi/4)
                elif keys[self.controls[1]] and keys[self.controls[2]]:
			move[0] -= self.velocity
			move[1] += self.velocity
			self.direction_angle = degrees(-5*pi/4)
                elif keys[self.controls[1]] and keys[self.controls[3]]:
			move[0] += self.velocity
			move[1] += self.velocity
			self.direction_angle = degrees(-7*pi/4)
		elif keys[self.controls[0]]:				#W -> UP
			move[1] -= self.velocity
			self.direction_angle = degrees(-pi/2)
                elif keys[self.controls[1]]:				#S -> DOWN
                        move[1] += self.velocity
                        self.direction_angle = degrees(-3*pi/2)
                elif keys[self.controls[2]]:				#A -> LEFT
                        move[0] -= self.velocity
                        self.direction_angle = degrees(-pi)
                elif keys[self.controls[3]]:				#D -> RIGHT
                        move[0] += self.velocity
                        self.direction_angle = degrees(0)

		#update the position of the tank based on the movement defined above	
		backup_rect = self.rect
                self.rect = self.rect.move(move[0], move[1])

		#if the movement results in a collision, go back to the position the tank was previously in
		if pygame.sprite.spritecollide(self, self.gs.level.colliders, False):
			self.rect = backup_rect

		#if the tanks collide, return them to their previous positions
		if self.number is 1:
			if pygame.sprite.spritecollide(self, self.gs.player2group, False) and self.gs.player2.alive:
				self.rect = backup_rect
		else:
			if pygame.sprite.spritecollide(self, self.gs.player1group, False) and self.gs.player1.alive:
				self.rect = backup_rect

		#get position of the mouse for turret angle
		mx, my = pygame.mouse.get_pos()

		#fire delay prevents steady stream of shell fire; can only fire one shell every 45 ticks
		if self.fire_delay > 0:
			self.fire_delay = self.fire_delay - 1

		if pygame.mouse.get_pressed()[0] and self.fire_delay <= 0:
			self.fire_delay = 45
			self.firing = True
		else:
			self.firing = False

		#plays the shell firing sound whenever a shell is fired
		if self.firing == True and self.alive:
			self.fire_noise.play()

			#Code to fire here
			if self.number is 1:
				self.gs.bullet_list1.append(Bullet(self.turret_rect.center[0], self.turret_rect.center[1], self.turret_direction_angle, self.number, self.gs))
			else:
				self.gs.bullet_list2.append(Bullet(self.turret_rect.center[0], self.turret_rect.center[1], self.turret_direction_angle, self.number, self.gs))

		#Code to get angle of turret using previously captured mouse position
		direction_vector = (mx - self.turret_rect.center[0], self.turret_rect.center[1] - my)

		if direction_vector[0] > 0 and direction_vector[1] > 0:	#Q1
			self.temp_ratio = float(direction_vector[0]) / float(direction_vector[1])
			self.turret_direction_angle = degrees(atan(self.temp_ratio)) 
		if direction_vector[0] < 0 and direction_vector[1] > 0:	#Q2
			self.temp_ratio = float(direction_vector[1]) / float(direction_vector[0])	
			self.turret_direction_angle = 270.0 - degrees(atan(self.temp_ratio)) 
		if direction_vector[0] < 0 and direction_vector[1] < 0:	#Q3
			self.temp_ratio = float(direction_vector[1]) / float(direction_vector[0])	
			self.turret_direction_angle = 270.0 - degrees(atan(self.temp_ratio)) 
		if direction_vector[0] > 0 and direction_vector[1] < 0:	#Q4
			self.temp_ratio = float(direction_vector[0]) / float(direction_vector[1])	
			self.turret_direction_angle = 180.0 + degrees(atan(self.temp_ratio)) 

		#rotates the tank according to its direction of movement
		self.center = self.rect.center
		self.image = pygame.transform.rotate(self.orig_image, - (self.direction_angle))
		self.rect = self.image.get_rect()	
		self.rect.center = self.center	

		#rotates the turret according to angle calculated with mouse position
		self.turret_image = pygame.transform.rotate(self.turret_orig_image, - (self.turret_direction_angle-90))
		self.turret_rect = self.turret_image.get_rect()
		self.turret_rect.center = self.rect.center

	def setPosition(self, x, y, angle = -1, turret_angle = -1):
		""" Function used to update the position of the opponenets tank (same as roatation code in player tick function) """
		self.rect.center = (x, y)
		if angle is not -1:
			self.direction_angle = angle
		if turret_angle is not -1:
			self.turret_direction_angle = turret_angle

		self.center = self.rect.center
		self.image = pygame.transform.rotate(self.orig_image, - (self.direction_angle))
		self.rect = self.image.get_rect()	
		self.rect.center = self.center	
		
		self.turret_image = pygame.transform.rotate(self.turret_orig_image, - (self.turret_direction_angle-90))
		self.turret_rect = self.turret_image.get_rect()
		self.turret_rect.center = self.rect.center

	def move(self, keys):
		move = [0, 0]
		if keys[self.controls[0]]:	#W -> UP
				move[1] -= self.velocity
		if keys[self.controls[1]]:	#S -> DOWN
				move[1] += self.velocity
		if keys[self.controls[2]]:	#A -> LEFT
				move[0] -= self.velocity
		if keys[self.controls[3]]:	#D -> RIGHT
				move[0] += self.velocity

		self.rect = self.rect.move(move[0], move[1])

	def got_hit(self):
		""" Function called when shell collides with tank. Adjusts health, health bar image, and resets damage cooldown """
		if self.damage_cooldown == 0:
			self.health = self.health - 1
			self.current_health_img = self.hp_images[self.health]
			if self.health < 1:
				self.alive = False
				if self.number == 1:
					self.gs.bluewins = True
				else:
					self.gs.redwins = True
			self.damage_cooldown = 30

	def update_opponent_health(self, health):
		""" Function necessary for updating opponent's health, health bar, and potential victory message """
		if health < self.health:
			self.health = health
			self.current_health_img = self.hp_images[self.health]
	
		if self.health <= 0:
			if self.number == 1:
				self.gs.bluewins = True
			else:
				self.gs.redwins = True

	def emit_fire_noise(self):
		""" Plays shell fire noise """
		pass
		self.fire_noise.play()


class Bullet(pygame.sprite.Sprite):
	#Class that holds shell attributes, including current position and trajectory
	def __init__(self, x, y, angle, owner, gs=None):
		pygame.sprite.Sprite.__init__(self)
		
		self.gs = gs
		
		self.live = True	#has not collided yet
		self.owner = owner		

		#angle in degrees
		self.x = float(x) + sin(radians(angle))*23
		self.y = float(y) - cos(radians(angle))*23
		self.angle = float(angle)
		self.velocity = 6.0
		
		# Load Image
		self.image = pygame.image.load(images_directory + "bullet1.png")
		self.image = pygame.transform.rotate(self.image, (90.0-angle))
		self.rect = self.image.get_rect()		
		self.rect.center = (self.x, self.y)
		

	def tick(self):
		#update position in fixed direction
		yinc = float(cos(radians(self.angle))) * float(self.velocity)
		xinc = float(sin(radians(self.angle))) * float(self.velocity)
		self.x += xinc
		self.y -= yinc
		self.rect.center = (self.x, self.y)
		
class Explosion(pygame.sprite.Sprite):
	#Class that handles a player's tank losing all health
	#Collection of images that play in a gif-like format
	def __init__(self, x, y, length, gs=None):
		pygame.sprite.Sprite.__init__(self)
	
		self.gs = gs

		self.x = x
		self.y = y
		self.length = length		#length is tick interval of each frame

		self.running = False		

		#sound effect of the explosion
		self.sound = pygame.mixer.Sound(sounds_directory + "explode.wav") 

		# Load all images
		self.images = []
		self.images.append(pygame.image.load(images_directory + "explosion/frames016a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames000a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames001a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames002a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames003a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames004a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames005a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames006a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames007a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames008a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames009a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames010a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames011a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames012a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames013a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames014a.png"))	
		self.images.append(pygame.image.load(images_directory + "explosion/frames015a.png"))

		#initializes starting image and its position
		self.current_frame = 0	
		self.interval = 0
		self.image = self.images[self.current_frame]
		self.rect = self.image.get_rect()
		self.rect.center = (self.x, self.y)

	def start(self):
		self.running = True
		self.sound.play()

	def tick(self):
		""" Function that ticks through the explosion, going through the list of frames until all have been displayed """
		if self.running:
			self.interval += 1
			if self.interval > self.length:
				self.current_frame += 1
				if self.current_frame < 16:
					self.image = self.images[self.current_frame]
				else:
					self.running = False
				self.interval = 0
