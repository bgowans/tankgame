# README #

This README will describe how to run TankGame on your machine.
Creators: Bill Gowans, Kurt Davis


# DEPENDENCIES #

First, be sure to have Python 2.6 (version on student machines), PyGame, and Twisted libraries installed.


# HOW TO RUN #

TankGame is a two-player game that runs on two separate machines (each player on his own machine). 
It runs as a peer-to-peer game where one machine acts as a host while the other connects to that host.


First, the host machine runs TankGame with the following command:

	python tankgame.py host <level file>

where <level file> is one of the following .dat files in the repository:

	leveltest.dat
	crossfire.dat
	mudpit.dat
	bunkers.dat


After the host machine is running TankGame, the client/connecting machine runs it with the following command:

	python tankgame.py client <host name>

host name being the host name of the first machine.


By default, TankGame runs through port 40054, however you can change the port in tankgame.py if desired by changing the global variable CLIENT_1_PORT.


# HOW TO PLAY #

Players control their tank with the keys W, A, S, and D to move up, left, down, and right, respectively, and can press multiple keys to move diagonally.
Players aim their cannons with the mouse and fire shells by left clicking the mouse. 
Holding down the left click provides continuous, but not anymore frequent, shell fire.
Players have health bars corresponding to the color of their tanks. Getting hit by an enemy shell depletes part of your helath bar.
When a player fully depletes the enemy tank's health bar, a victory message is displayed and the winning tank is free to do victory laps.
The game ends when the host machine exits out of the game window.
